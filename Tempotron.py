import numpy as np

class Tempotron(object):

	def __init__(self, length, synapses, Cm=4.9, Vreset=-63., Vthresh=-55., Vrest=-58., V0=2.12, Erev=-80., El=-58., gl=.02):
		self.Vreset = float(Vreset) # mV
		self.Vthresh = float(Vthresh) # mV
		self.Vrest = float(Vrest) # mV
		self.V0 = float(V0)
		self.Erev = float(Erev) # mV
		self.El = float(El) # mV
		self.gl = float(gl) # uS

		self.length = length
		self.synapses = synapses

		# self.Rm = 1/gl
		# self.taum = Cm*self.Rm

		self.tau = 15 # in ms
		self.taus = self.tau/4

		self.weights = np.zeros(synapses,length)

	def __PSP(self, t):

		return self.V0*(m.exp(-t/self.tau) - m.exp(-t/self.taus))

	def V(self, t):

		return self.weights[t]*(self.__PSP(t) + self.Vrest)

	def __cost(self, t, data):
		Vthresh = self.Vthresh
		spike = 1 if np.count_nonzero(data) > 0 else 0
		tmax = self.length - 1

		if (spike == 1):
			return Vthresh - self.V(tmax)
		elif (spike == 0):
			return self.V(tmax) - Vthresh

	def __spikeoutput(self, t):
		if (abs(self,V(t)) > abs(self.Vthresh)):
			return 1
		else:
			return 0
	
	def __syn_update(self, t, maxsize=.00008):

		tmax = self.length - 1
		return maxsize*self.__PSP(tmax - t)

	def train(self, data):

		for t, d in enumerate(data):
			expected = d
			target = self.__spikeoutput(t)
			if (target != expected):
				dw = self.__syn_update(t)
				if (expected == 0):
					self.weights[t] -= dw
				else:
					self.weights[t] += dw


	def test(self, data):

		for t, d in enumerate(data):
			if (abs(self.V(t)) > abs(self.Vthresh)):
				return 1
		return 0
