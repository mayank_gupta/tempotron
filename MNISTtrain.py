import brian2 as brian
from mnist import MNIST
from MNISTImage import MNISTImage
import numpy as np
import sys
from random import randrange

nencoding=98
nlearning=4
pixelsPerNeuron=8
debug = False
def knuth_shuffle(x):
	for i in range(len(x)-1, 0, -1):
		j = randrange(i + 1)
		x[i], x[j] = x[j], x[i]

def printProgress (iteration, total, prefix = '', suffix = '', decimals = 2, barLength = 100):
	"""
	Call in a loop to create terminal progress bar
	@params:
	iterations  - Required  : current iteration (Int)
	total       - Required  : total iterations (Int)
	prefix      - Optional  : prefix string (Str)
	suffix      - Optional  : suffix string (Str)
	"""
	filledLength    = int(round(barLength * iteration / float(total)))
	percents        = round(100.00 * (iteration / float(total)), decimals)
	bar             = '#' * filledLength + '-' * (barLength - filledLength)
	sys.stdout.write('%s [%s] %s%s %s\r' % (prefix, bar, percents, '%', suffix)),
	sys.stdout.flush()
	if iteration == total:
		print("\n")

def loadMNISTData():
	mndata = MNIST('./python-mnist/data')
	mndata.load_training()
	mndata.load_testing()
	return mndata

def getTrainData(mndata, target, maxsamples,encoding):
	train_data = []
	nsamples = 0
	for index,label in enumerate(mndata.train_labels):
		if label in target:
			img=mndata.train_images[index]
			mimage=MNISTImage(label,img,pixelsPerNeuron,256,encoding)
			train_data.append(mimage)
			nsamples += 1
		if nsamples >= maxsamples:
			break
	return train_data

def getTestData(mndata, target, maxsamples,encoding):
	test_data = []
	nsamples = 0
	for index,label in enumerate(mndata.test_labels):
		if label in target:
			img=mndata.test_images[index]
			mimage=MNISTImage(label,img,pixelsPerNeuron,256,encoding)
			test_data.append(mimage)
			nsamples += 1
		if nsamples >= maxsamples:
			break
	return test_data

def plot(Mlearning, Mencoding, Sencoding, MSynapse,SencodingQ):
	fig1 = brian.figure('Learning layer voltage')
	brian.plot(Mlearning.t/brian.ms,Mlearning.v.T/brian.mV)
	fig1.suptitle('Learning layer voltage', fontsize=20)
	fig2 = brian.figure('Encoding layer voltage')
	brian.plot(Mencoding.t/brian.ms,Mencoding.v.T/brian.mV)
	fig2.suptitle('Encoding layer voltage', fontsize=20)
	fig3 = brian.figure('Encoding layer spike')
	brian.plot(Sencoding.t/ms,Sencoding.i,'*')
	fig3.suptitle('Encoding layer spike', fontsize=20)
	fig4 = brian.figure('Synaptic weights')
	brian.plot(MSynapse.t / ms, MSynapse[0].w)
	fig4.suptitle('Synaptic weights', fontsize=20)
	fig5 = brian.figure('Learning layer spike')
	brian.plot(SencodingQ.t/ms,SencodingQ.i,'.')
	fig5.suptitle('Learning layer spike', fontsize=20)

def visualise_connectivity(S):
	Ns = len(S.source)
	Nt = len(S.target)
	brian.figure('Network connectivity',figsize=(10, 4))
	brian.subplot(121)
	brian.plot(brian.zeros(Ns), brian.arange(Ns), 'ok', ms=10)
	brian.plot(brian.ones(Nt), brian.arange(Nt), 'ok', ms=10)
	for i, j in zip(S.i, S.j):
		brian.plot([0, 1], [i, j], '-k')
		brian.xticks([0, 1], ['Source', 'Target'])
		brian.ylabel('Neuron index')
		brian.xlim(-0.1, 1.1)
		brian.ylim(-1, max(Ns, Nt))
		brian.subplot(122)
		brian.plot(S.i, S.j, 'ok')
	brian.xlim(-1, Ns)
	brian.ylim(-1, Nt)
	brian.xlabel('Source neuron index')
	brian.ylabel('Target neuron index')




ms=brian.ms
mV=brian.mV

indices = brian.array([0, 2, 1])
times = brian.array([1, 6, 14])*brian.ms

encodingth=-64*mV
vth=-60*mV
vrest=-65*mV
vo1 = 2*mV
taum=15*ms
taus=(15/4)*ms
timewindow = 256*ms




encoding_model = brian.Equations('''tspike: second
	vo: volt (shared)
	tstart: second (shared)
    	psp= vo*int(tspike >= tstart)*int(tspike <= t)*(exp(-(t-tspike)/taum)-exp(-(t-tspike)/taus)) : volt
	v =vrest + psp : volt''')

synapse_model = '''w : 1
	plastic: boolean (shared)
	psp_post = w*psp_pre : volt (summed)
        didspike = int(vrest + psp_post > vth) : 1
        expected = int(t >= tmax-1)*target_post : 1
        dw = int(plastic)*(expected-didspike)*pconstant*psp_pre : 1
        tmax: second (shared)
        pconstant : 1 (shared)
        nconstant : 1 (shared)'''
        
synapse_learning_spike = '''
    w=w+dw
'''
synapse_encoding_spike = '''
    w=w+dw
'''

    
learning_model = brian.Equations('''
			psp : volt
			v = vrest + psp: volt
			target: 1''')


def learn(encoding_layer, learning_layer, synapse, data, target):
	synapse.plastic = True
	encoding_layer.tspike=data
	encoding_layer.vo = vo1
	learning_layer.target=target
	brian.run(timewindow)

def evaluate(encoding_layer, learning_layer, synapse, data):
	synapse.plastic = False
	encoding_layer.tspike=data
	encoding_layer.vo = vo1
	learning_layer.target=np.zeros(nlearning)
	brian.run(timewindow)
	return learning_layer.target

P = brian.NeuronGroup(nencoding,model=encoding_model,threshold='v > encodingth')
#P.tspike=times
P.vo = vo1
P.tstart = 0

Q = brian.NeuronGroup(nlearning,model=learning_model,threshold='v > vth')

S=brian.Synapses(P,Q,model=synapse_model,pre=synapse_encoding_spike,post=synapse_learning_spike)
S.connect('i!=j')
S.w=1
S.tmax=timewindow
S.pconstant=0.1
S.nconstant=-S.pconstant
S.plastic = True

if (debug == True):
	Mlearning = brian.StateMonitor(Q,'v',record=range(nlearning))
	Mencoding=brian.StateMonitor(P,'v',record=range(nencoding))
	Sencoding = brian.SpikeMonitor(P)
	SencodingQ = brian.SpikeMonitor(Q)
	MSynapse = brian.StateMonitor(S,'w',record=S[0,:])

mndata = loadMNISTData()
indices = range(0,784)
#knuth_shuffle(indices)
train_data1 = getTrainData(mndata,[1,8],10000,indices)
#train_data8 = getTrainData(mndata,[8],10000,indices)
knuth_shuffle(train_data1)
test1 =  getTestData(mndata,[1],1,indices)
test8 =  getTestData(mndata,[8],1,indices)

def trainTempotron(trainingImage, P, Q, S):
	spike_times = trainingImage.spike_times*ms + P.tstart
	label = trainingImage.getTargetLabel(4)
	learn(P,Q,S,spike_times,label)
	P.tstart = P.tstart + timewindow
	S.tmax = S.tmax + timewindow
	return


def trainBatch(train_data,P,Q,S):
	l=len(train_data)
	for i in range(0,l):
		trainTempotron(train_data[i],P,Q,S)
		printProgress(i, l-1, prefix = 'Iterations', suffix = 'Complete', barLength = 50)
	#print("Batch ended")

def getLabel(test_image,P,Q,S):
	spike_times = test_image.spike_times*ms + P.tstart
	mmm = brian.SpikeMonitor(Q)
	l = evaluate(P,Q,S,spike_times)
	print(mmm)
	P.tstart = P.tstart + timewindow
	S.tmax = S.tmax + timewindow
	return l

trainBatch(train_data1,P,Q,S)
#trainBatch(train_data8,P,Q,S)

print(getLabel(test1[0],P,Q,S))
print(getLabel(test8[0],P,Q,S))
#visualise_connectivity(S)
np.save('synapticweights.txt',S.w)

if (debug == True):
	plot(Mlearning, Mencoding, Sencoding,MSynapse,SencodingQ)
	brian.show()




