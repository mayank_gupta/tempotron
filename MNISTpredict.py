import brian2 as brian
from mnist import MNIST
from MNISTImage import MNISTImage
import numpy as np

nencoding=98
nlearning=4
pixelsPerNeuron=8

filename='synapticweights.txt.npy'
synapticweights = np.load(filename)

print(synapticweights)


ms=brian.ms
mV=brian.mV

indices = brian.array([0, 2, 1])
times = brian.array([1, 6, 14])*brian.ms

encodingth=-64*mV
vth=-60*mV
vrest=-65*mV
vo1 = 2*mV
taum=15*ms
taus=(15/4)*ms
timewindow = 256*ms

def loadMNISTData():
	mndata = MNIST('./python-mnist/data')
	mndata.load_training()
	mndata.load_testing()
	return mndata



def plot(Mlearning, Mencoding, Sencoding, MSynapse,SencodingQ):
	fig1 = brian.figure('Learning layer voltage')
	brian.plot(Mlearning.t/brian.ms,Mlearning.v.T/brian.mV)
	fig1.suptitle('Learning layer voltage', fontsize=20)
	fig2 = brian.figure('Encoding layer voltage')
	brian.plot(Mencoding.t/brian.ms,Mencoding.v.T/brian.mV)
	fig2.suptitle('Encoding layer voltage', fontsize=20)
	fig3 = brian.figure('Encoding layer spike')
	brian.plot(Sencoding.t/ms,Sencoding.i,'*')
	fig3.suptitle('Encoding layer spike', fontsize=20)
	fig4 = brian.figure('Synaptic weights')
	brian.plot(MSynapse.t / ms, MSynapse[0].w)
	fig4.suptitle('Synaptic weights', fontsize=20)
	fig5 = brian.figure('Learning layer spike')
	brian.plot(SencodingQ.t/ms,SencodingQ.i,'.')
	fig5.suptitle('Learning layer spike', fontsize=20)


def getTestData(mndata, target, maxsamples,encoding):
	test_data = []
	nsamples = 0
	for index,label in enumerate(mndata.test_labels):
		if label in target:
			img=mndata.test_images[index]
			mimage=MNISTImage(label,img,pixelsPerNeuron,256,encoding)
			test_data.append(mimage)
			nsamples += 1
		if nsamples >= maxsamples:
			break
	return test_data


encoding_model = brian.Equations('''tspike: second
	vo: volt (shared)
	tstart: second (shared)
    	psp= vo*int(tspike >= tstart)*int(tspike <= t)*(exp(-(t-tspike)/taum)-exp(-(t-tspike)/taus)) : volt
	v =vrest + psp : volt''')

synapse_model = '''w : 1
	plastic: boolean (shared)
	psp_post = w*psp_pre : volt (summed)
        didspike = int(vrest + psp_post > vth) : 1
        expected = int(t >= tmax-1)*target_post : 1
        dw = int(plastic)*(expected-didspike)*pconstant*psp_pre : 1
        tmax: second (shared)
        pconstant : 1 (shared)
        nconstant : 1 (shared)'''
        
synapse_learning_spike = '''
    w=w+dw
'''
synapse_encoding_spike = '''
    w=w+dw
'''

    
learning_model = brian.Equations('''
			psp : volt
			v = vrest + psp: volt
			target: 1''')


def learn(encoding_layer, learning_layer, synapse, data, target):
	synapse.plastic = True
	encoding_layer.tspike=data
	encoding_layer.vo = vo1
	learning_layer.target=target
	brian.run(timewindow)

def evaluate(encoding_layer, learning_layer, synapse, data):
	synapse.plastic = False
	encoding_layer.tspike=data
	encoding_layer.vo = vo1
	learning_layer.target=np.zeros(nlearning)
	brian.run(timewindow)
	return learning_layer.target

P = brian.NeuronGroup(nencoding,model=encoding_model,threshold='v > encodingth')
P.vo = vo1
P.tstart = 0

Q = brian.NeuronGroup(nlearning,model=learning_model,threshold='v > vth')

S=brian.Synapses(P,Q,model=synapse_model,pre=synapse_encoding_spike,post=synapse_learning_spike)
S.connect('i!=j')
S.w=synapticweights 
S.tmax=timewindow
S.pconstant=0.1
S.nconstant=-S.pconstant
S.plastic = False

indices=range(0,784)
mndata = loadMNISTData()
testdata1 = getTestData(mndata,[1],1,indices)
testdata2 = getTestData(mndata,[8],1,indices)


def getLabel(test_image,P,Q,S):
	spike_times = test_image.spike_times*ms + P.tstart
	mmm = brian.SpikeMonitor(Q)
	l = evaluate(P,Q,S,spike_times)
	print(mmm)
	P.tstart = P.tstart + timewindow
	S.tmax = S.tmax + timewindow
	return l

l = getLabel(testdata1,P,Q,S)
