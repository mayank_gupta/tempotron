from random import randrange
import numpy as np

class MNISTImage(object):

	def __init__(self, digit, data=None):
		self.digit = digit
		# self.image = image
		self.data = data
		self.encoded_data = self.encode()

	def encode(self):

		data_array = np.array([1 if x > 0 else 0 for x in self.data])
		def knuth_shuffle(x):
		    for i in range(len(x)-1, 0, -1):
			j = randrange(i + 1)
			x[i], x[j] = x[j], x[i]


		def getSpikeTime (data, indices, pixelPerNeuron,encoding='linear'):
		    if encoding == 'random':
				knuth_shuffle(indices)
		    spiketimes = []
		    sampledindices = []
		    for i in range(0,len(data),pixelPerNeuron):
				spiketime = int("".join(map(str,[data[indices[j]] for j in range(i,i+pixelPerNeuron)])),2)
				spiketimes.append(spiketime)
				sampledindices.append([indices[j] for j in range(i,i+pixelPerNeuron)])

		    return spiketimes, sampledindices

		timewindow=16
		pixelPerNeuron = 4
		indices = range(0,len(data_array))
		spiketimes,sampledindices = getSpikeTime(data_array,indices,pixelPerNeuron,'linear')
		encoded=[]
		print spiketimes
		for spiketime in spiketimes:
			spiketrain = np.zeros(timewindow)
			if (spiketime !=0):
				spiketrain[spiketime]=1
			encoded.append(spiketrain)


		return encoded
