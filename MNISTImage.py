import numpy as np


class MNISTImage:
    def __init__(self, d, im, pn, tw, encoding):
        self.digit = d
        self.image = im
        self.pixelsPerNeuron = pn
        self.timewindow = tw
        self.encoding = encoding
        self.spike_times = self.spikes()
        self.encoded_data = self.encode()

    def getTargetLabel(self, seqLen):
        binarystr = np.binary_repr(self.digit)
        binaryarr = np.fromstring(binarystr, 'u1') - ord('0')
        extrazeros = np.zeros(seqLen - len(binaryarr))
        return np.concatenate((extrazeros, binaryarr))

    def spikes(self):
        data_array = np.array([1 if x > 0 else 0 for x in self.image])

        def getSpikeTime(data, indices, pixelPerNeuron, encoding='linear'):
            spiketimes = []
            sampledindices = []
            for i in range(0, len(data), pixelPerNeuron):
                spiketime = int("".join(map(str, [data[indices[j]] for j in range(i, i + pixelPerNeuron)])), 2)
                spiketimes.append(spiketime)
                sampledindices.append([indices[j] for j in range(i, i + pixelPerNeuron)])

            return spiketimes, sampledindices

        spiketimes, sampledindices = getSpikeTime(data_array, self.encoding, self.pixelsPerNeuron, 'random')
        return spiketimes

    def encode(self):
        encoded = []
        for spiketime in self.spike_times:
            spiketrain = np.zeros(self.timewindow)
            spiketrain[spiketime] = 1 if spiketime > 0 else 0
            encoded.append(spiketrain)

        return encoded
